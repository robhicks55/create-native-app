const defaultName = 'native-app';
const fs = require('fs-extra');
const ora = require('ora');
const packageJsonGenerator = require('./packageJsonGenerator.js');
const shell = require('shelljs');

module.exports = dir => {
  const spinner = ora('installing packages').start();
  return Promise.resolve()
    .then(() => (dir ? shell.cd(`./${dir}`) : Function))
    .then(() => fs.writeFile('package.json', JSON.stringify(packageJsonGenerator(dir || defaultName)), 'utf8'))
    .then(() => shell.exec('npm i'))
    .catch(err => Promise.reject(err))
    .finally(() => spinner.stop())
    .finally(() => (dir ? shell.cd('../') : Function));
};
