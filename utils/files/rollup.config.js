import nodeResolve from 'rollup-plugin-node-resolve';
import terser from 'rollup-plugin-terser';
import liveServer from 'rollup-plugin-live-server';
import { resolve } from 'path';
import commonJs from 'rollup-plugin-commonjs';

const input = resolve(__dirname, 'src', 'native-app.js');

let tasks = [];

const devTasks = [
  {
    input,
    plugins: [
      nodeResolve(),
      commonJs(),
      liveServer({
        port: 3000,
        host: '0.0.0.0',
        root: './public',
        open: false,
        wait: 500
      })
    ],
    output: {
      file: resolve(__dirname, 'public'),
      format: 'es'
    },
    watch: {
      chokidar: true,
      clearScreen: true,
      include: resolve(__dirname, 'src', '**')
    }
  }
];

const prodTasks = [

];

if (process.argv.some(key => key === '-cw' || key === '-wc')) tasks = [ ...tasks, ...devTasks ];
else tasks = [ ...tasks, ...prodTasks ];

export default tasks;
