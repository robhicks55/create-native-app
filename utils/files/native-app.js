class NativeApp extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: 'open' });
  }

  connectedCallback() {

  }
}

customElement.get('native-app') || customElements.define('native-app', NativeApp);

export { NativeApp };
