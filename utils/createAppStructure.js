const fs = require('fs-extra');
const indexHtmlGenerator = require('./indexHtmlGenerator.js');
const ora = require('ora');
const shell = require('shelljs');
const { resolve } = require('path');
const root = process.cwd();

module.exports = dir => {
  const spinner = ora('creating app structure').start();
  return Promise.resolve()
    .then(() => (dir ? shell.cd(`./${dir}`) : Function))
    .then(() => shell.mkdir('src'))
    .then(() => shell.mkdir('public'))
    .then(() => fs.writeFile('public/index.html', indexHtmlGenerator(), 'utf8'))
    .then(() => fs.copy(resolve(root, 'utils', 'files', 'manifest.json'), resolve('public', 'manifest.json')))
    .then(() => fs.copy(resolve(root, 'utils', 'files', 'favicon.ico'), resolve('public', 'favicon.ico')))
    .then(() => fs.copy(resolve(root, 'utils', 'files', 'rollup.config.js'), 'rollup.config.js'))
    .then(() => fs.copy(resolve(root, 'utils', 'files', 'native-app.js'), resolve('src', 'native-app.js')))

    .catch(err => Promise.reject(err))
    .finally(() => spinner.stop())
    .finally(() => (dir ? shell.cd('../') : Function));
};
