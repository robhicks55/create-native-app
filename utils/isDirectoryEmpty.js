const fs = require('fs-extra');

module.exports = () => fs.readdir('.').then(files => !files.length);
