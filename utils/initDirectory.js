const ora = require('ora');
const shell = require('shelljs');

module.exports = dir => {
  const spinner = ora().start();
  return Promise.resolve()
    .then(() => (dir ? shell.cd(`./${dir}`) : Function))
    .then(() => shell.which('git'))
    .then(() => shell.which('npm'))
    .then(() => shell.exec('git init'))
    .then(() => (dir ? shell.cd('../') : Function))
    .catch(err => Promise.reject(err))
    .finally(() => spinner.stop());
};
