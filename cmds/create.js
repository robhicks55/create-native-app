const fs = require('fs-extra');
const initDirectory = require('../utils/initDirectory.js');
const installPackages = require('../utils/installPackages.js');
const createAppStructure = require('../utils/createAppStructure.js');

module.exports = args => {
  const [ appName ] = args._;
  console.log(`creating app ${appName ? appName : ''}`);

  if (appName) {
    fs.ensureDir(`./${appName}`)
      .then(() => initDirectory(appName))
      .then(() => installPackages(appName))
      .then(() => createAppStructure(appName));

  } else {
    const isDirectoryEmpty = require('../utils/isDirectoryEmpty.js');
    isDirectoryEmpty()
      .then(answer => (answer ? answer : Promise.reject(Error('cannot create app in directory which contains files'))))
      .then(() => initDirectory())
      // .then(() => require('../utils/installPacakges')())
      .catch(err => console.error(err));
  }
};
