global.Promise = require('bluebird');
Promise.promisifyAll(require('shelljs'));
const minimist = require('minimist');

module.exports = () => {
  const args = minimist(process.argv.slice(2));
  let cmd  = 'create';
  if (args.help || args.h) cmd = 'help';
  if (args.version || args.v) cmd = 'version';

  switch (cmd) {
    case 'create': require('./cmds/create')(args); break;
    case 'help': require('./cmds/help')(args); break;
    case 'version': require('./cmds/version')(args); break;
    default: console.error(`"${cmd}" is not a valid command!`);
  }
};
